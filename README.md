# Vuleticd_EcosystemWebsites

Magento Ecosystem entity adapter for Magento Websites.
Enables Magento websites data to be shared between Ecosystem enabled websites.
For exchange to work, this extension is required on both source and target websites. 

## Requirements

Vuleticd_Ecosystem

## Features

* Define ecosystem entity adapter for Magento websites.
* Introduce custom Magento API endpoint ecosystem_website.info (V1), ecosystemWebsiteInfo (V2), which returns website data from source, based on code value provided.
* Set **code** as unique entity attribute shared between source and target websites.
* if default store group from source website is not found on target website, module will not change default store group value on target website. Store groups are compared by name. 
