<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category    Vuleticd
 * @package     Vuleticd_EcosystemWebsites
 * @copyright   Copyright (c) 2013 Vuletic Dragan (http://www.vuleticd.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Vuleticd_EcosystemWebsites_Model_Api extends Mage_Api_Model_Resource_Abstract
{
    /**
     * Retrieve website data
     *
     */
	public function info($websiteCode)
    {
        // Retrieve website info
        try {
            $website = Mage::getModel('core/website')->load($websiteCode);
        } catch (Exception $e) {
            $this->_fault('website_not_exists');
        }

        if (!$website->hasWebsiteId()) {
            $this->_fault('website_not_exists');
        }
        // Basic website data
        $result = array();
        
        $result['code'] = $website->getCode();
        $result['name'] = $website->getName();
        $result['sort_order'] = $website->getSortOrder();
        $result['default_group_name'] = Mage::getModel('core/store_group')->load($website->getDefaultGroupId())->getName();
        $result['is_default'] = $website->getIsDefault();
        $result['is_staging'] = $website->getIsStaging();

        $result['master_login'] = $website->getMasterLogin();
        $result['master_password'] = $website->getMasterPassword();
        $result['visibility'] = $website->getVisibility();
        
        return $result;
    }
}